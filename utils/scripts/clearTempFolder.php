<?php

date_default_timezone_set('Europe/Paris');

echo sprintf(
    "Clean temporary folder : script started at %s\n",
    date('d/m/Y h:i:s')
);

$timeStart = microtime(true);

$tempDir = 'public/img/tmp/';
$tempFiles = array_diff(scandir($tempDir), ['.', '..']);
$fileSize = 0;

foreach ($tempFiles as $tempFile) {
    $filePath = $tempDir . $tempFile;
    $fileSize += filesize($filePath) / 1000000;
    unlink($filePath);
}

echo sprintf(
    'Deleted %s file%s in %s ms (%s mo freed)',
    count($tempFiles),
    count($tempFiles) < 2 ? '' : 's',
    round((microtime(true) - $timeStart) * 1000, 2),
    round($fileSize, 2)
);

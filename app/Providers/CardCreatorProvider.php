<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

use Exception;

define('_ASSETS_DIR', resource_path() . '/assets/');
define('_IM_DIR', _ASSETS_DIR . 'img/');
define('_FONT_DIR', _ASSETS_DIR . 'fonts/');

define('_TEMP_IM_DIR', 'img/tmp/');
define('_PUB_IM_DIR', 'img/published/');
define('_MISC_DIR', '/src/');

//include_once 'symbol_conversion.php';
//include_once 'utils.php';

class CardCreatorProvider extends Serviceprovider
{

    static public function getImDir(): string
    {
//        return IM_DIR;
        return '';
    }


    static public function usr_log(string $log): void
    {
        global $outPutData;
        array_push($outPutData['log'], $log);
    }


    /**
     * @return string[]
     */
    static public function processTextSymbols(string $text, int $fontSize, array $fonts, $textType = 'main'): array
    {
        $processedOutput = [];

        $bold = false;
        $italic = false;

        // TODO replace '\n' par '\n '

        $cardTextArray = explode(' ', $text);

        $i = 0;

        foreach ($cardTextArray as $word) {

            $word = $cardTextArray[$i]; // ?

            $currentWord = [];

            // ----------------------------------- Bold/Italic text processing

            if (strlen($word) > 0 && substr($word, 0, 1) == '*') {
                if (strlen($word) > 1 && substr($word, 0, 2) == '**') {
                    $bold = !$bold;
                    $word = substr($word, 2);
                } else {
                    $italic = !$italic;
                    $word = substr($word, 1);
                }
            }

            // -----------------------------------

            if ($word == '') {

            } else {
                if (preg_match("/![^\s]{3}$/", $word)) {
                    $symbName = substr($word, 1);
                    $currentWord = ['text' => SymbolConversionProvider::symbolToChar($symbName), 'font' => $fonts['symbol'], 'fontSize' => $fontSize];
                } else {
                    if ($bold && $italic) {
                        $currentWord = ['text' => $word, 'font' => $fonts['boldItalic'], 'fontSize' => $fontSize];
                    } else if ($bold) {
                        $currentWord = ['text' => $word, 'font' => $fonts['bold'], 'fontSize' => $fontSize];
                    } else if ($italic) {
                        $currentWord = ['text' => $word, 'font' => $fonts['italic'], 'fontSize' => $fontSize];
                    } else {
                        $currentWord = ['text' => $word, 'font' => $fonts[$textType], 'fontSize' => $fontSize];
                    }
                }
                /*
                    - no space after [ before !xxx
                    - no space after !xxx before ( ] | ]: | . | , )
                    - after LIMITED, half space
                */
                if (
                    $word == '[' && $i + 1 < sizeof($cardTextArray) &&
                    preg_match("/![^\s]{3}$/", $cardTextArray[$i + 1])
                ) {
                    $currentWord['spaceAfter'] = 0;
                } else if (
                    preg_match("/![^\s]{3}$/", $word) &&
                    $i + 1 < sizeof($cardTextArray) &&
                    in_array($cardTextArray[$i + 1], [']', ']:', '.', ','])
                ) {
                    $currentWord['spaceAfter'] = 0;
                } else if ($word == '!LIM') {
                    $currentWord['spaceAfter'] = 0.5;
                } else {
                    $currentWord['spaceAfter'] = 1;
                }
                /*
                    if $word[max] == \n
                */
                if (false) {
                    $currentWord['cReturnAfter'] = true;
                } else {
                    $currentWord['cReturnAfter'] = false;
                }
                array_push($processedOutput, $currentWord);
            }

            $i++;
        }

        return $processedOutput;
    }

    /**
     * @param $backGroundImage
     * @param $areaLeftLimit
     * @param $areaRightLimit
     * @param $areaTopLimit
     * @param $areaBottomLimit
     * @param $content
     * @param $color
     */
    static public function writeTextBlockCenter($backGroundImage, $areaLeftLimit, $areaRightLimit, $areaTopLimit, $areaBottomLimit, $content, $color)
    {

        $y_newLine = 14;
        $x_space = 6;

        $lines = [];

        // ----------------------------------- Line by line splitting

        $currentLine = [];
        $currentWidthOfLine = 0;

        foreach ($content as $word) {

            $widhtOfNewWord = imagettfbbox($word['fontSize'], 0, $word['font'], $word['text'])[2] + $x_space;

            if ($currentWidthOfLine + $widhtOfNewWord >= $areaRightLimit - $areaLeftLimit) {
                array_push($lines, $currentLine);
                $currentLine = [];
                $currentWidthOfLine = 0;
            }

            array_push($currentLine, $word);
            $currentWidthOfLine += $widhtOfNewWord;

        }

        if ($currentLine != []) {
            array_push($lines, $currentLine);
        }


        // ----------------------------------- Calculating the top offset for vertical alignment

        $topOffset = ($areaBottomLimit - $areaTopLimit - sizeof($lines) * $y_newLine) / 2 + $y_newLine;

        // ----------------------------------- Writing text line by line

        foreach ($lines as $line) {
            self::writeLignCenter($backGroundImage, $areaLeftLimit, $areaRightLimit, $areaTopLimit + $topOffset, $areaBottomLimit, $line, $color);
            $topOffset += $y_newLine;
        }

    }


    /**
     * @param $backGroundImage
     * @param $areaLeftLimit
     * @param $areaRightLimit
     * @param $areaTopLimit
     * @param $areaBottomLimit
     * @param $line
     * @param $color
     */
    static public function writeLignCenter($backGroundImage, $areaLeftLimit, $areaRightLimit, $areaTopLimit, $areaBottomLimit, $line, $color)
    {

        $x_space = 6;

        // ----------------------------------- Left offset calculation

        $totalWidth = 0;
        foreach ($line as $word) {
            $totalWidth += imagettfbbox($word['fontSize'], 0, $word['font'], $word['text'])[2] + $x_space;
        }
        $totalWidth -= $x_space;
        $x_offset = ($areaRightLimit - $areaLeftLimit - $totalWidth) / 2;

        // ----------------------------------- Word by word drawing

        foreach ($line as $word) {
            // Draw word and return corners
            $corners = imagettftext($backGroundImage, $word['fontSize'], 0, $areaLeftLimit + $x_offset, $areaTopLimit, $color, $word['font'], $word['text']);
            $x_offset = $corners[2] - $areaLeftLimit;
            $x_offset += $word['spaceAfter'] * $x_space;
        }

    }

    /**
     * @param $backGroundImage
     * @param $areaLeftLimit
     * @param $areaRightLimit
     * @param $areaTopLimit
     * @param $areaBottomLimit
     * @param $size_h
     * @param $size_w
     * @param $newImage
     * @return array
     */
    static public function drawImageCenter($backGroundImage, $areaLeftLimit, $areaRightLimit, $areaTopLimit, $areaBottomLimit, $size_h, $size_w, $newImage)
    {

        $x_image = imagesx($newImage);
        $y_image = imagesy($newImage);

        $x_offset = ($areaRightLimit - $areaLeftLimit - $size_w) / 2;

        //$y_offsetCenter = ( $areaBottomLimit - ($areaTopLimit + imagettfbbox($fontSize, 0, $font, $text)[5] ) )/2;
        $y_offset = 0;

        $offsets = [$x_offset, $y_offset];

        imagecopyresampled($backGroundImage, $newImage, $areaLeftLimit + $x_offset, $areaTopLimit + $y_offset, 0, 0, $size_h, $size_w, $x_image, $y_image);

        return $offsets;
    }

    static public function checkMinMax(int $val, int $min, int $max): int
    {
        if (!is_numeric($val)) {
            $val = 0;
        }
        $v = $val < $max ? $val : $max;
        $v = $v > $min ? $v : $min;
        return $v;
    }

    /**
     * @param $fileName
     * @return false|resource
     */
    static public function loadImagePng($fileName)
    {
        $im = imagecreatefrompng(_IM_DIR . $fileName);
        return $im;
    }

    /**
     * @param $name
     * @return string
     * @throws Exception
     */
    static public function loadRessource(string $name)
    {
        switch (explode('.', $name)[1]) {
            case 'ttf':
                $dir = _FONT_DIR;
                break;
            default:
                $dir = _MISC_DIR;
                break;
        }
        if (file_exists($dir . $name)) {
            return $dir . $name;
        } else {
            throw new Exception('Ressource "' . $dir . $name . '" not found');
        }
    }

    /**
     * @return int
     */
    static public function getCardNewIndex()
    {
        // à refaire
//        $files = glob(public_path() . _PUB_IM_DIR . '*.{png}', GLOB_BRACE);
//        $index = 0;
//        foreach ($files as $file) {
//            $temp = explode('/', $file);
//            $cardName = array_pop($temp);
//            $cardName = explode('.', $cardName)[0];
//            $currentIndex = explode('_', $cardName)[2];
//            if ($currentIndex > $index) {
//                $index = $currentIndex;
//            }
//        }
        return 0;
    }

    static public function saveImageOnDiskTemp($content, string $id, bool $privatePath): string
    {
        $name = 'card-' . $id . '.png';
        $fullName = public_path() . '/' . _TEMP_IM_DIR . $name;
        file_put_contents($fullName, $content);
        if ($privatePath) {
            return $fullName;
        } else {
            return url(_TEMP_IM_DIR) . '/' . $name;
        }
    }

    public static function publishCard(string $cardTitle, string $user, string $session): string
    {
        // cardName_user_index.png
        $index = self::getCardNewIndex();
        $tempCardPath = public_path() . '/' . _TEMP_IM_DIR . 'card-' . $session . '.png';
        $publishedCardPath = _PUB_IM_DIR . $cardTitle . '_' . $user . '_' . $index . '.png';
        copy($tempCardPath, public_path() . '/' . $publishedCardPath);
        return url($publishedCardPath);
    }

    /**
     * @return string[]
     */
    static public function getPublishedCardsPath(): array
    {
        $files = glob(public_path() . '/' . _PUB_IM_DIR . '*.{png}', GLOB_BRACE);
        $relPath = [];
        foreach ($files as $file) {
            $temp = explode('/', $file);
            $cardName = array_pop($temp);
            array_push($relPath, url(_PUB_IM_DIR) . '/' . $cardName);
        }
        return $relPath;
    }

    public static function cleanTempImages()
    {

    }
}

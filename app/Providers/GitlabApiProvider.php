<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Exception;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class GitlabApiProvider extends ServiceProvider
{

    public static function getCommits(): array
    {
        $endpoint = "https://gitlab.com/api/v4/projects/15125207/repository/commits";
        $client = new Client(['verify' => false]);

        try {
            if (env('APP_ENV') !== 'production') {
                $endpoint .= '?ref_name=preproduction';
            }
            $response = $client->request('GET', $endpoint);
            $content = json_decode($response->getBody(), true);

            foreach ($content as $key => $commit) {
                if (strpos($commit['message'], "Merge branch") === 0) {
                    unset($content[$key]);
                }
            }
        } catch (Exception $e) {
            $content = [];
        }

        return $content;
    }

}

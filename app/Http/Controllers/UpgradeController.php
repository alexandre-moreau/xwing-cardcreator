<?php

namespace App\Http\Controllers;

use App\Providers\UpgradeCardCreatorProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class UpgradeController extends Controller
{
    public function index()
    {
        return view('upgrade.index', ['locales' => config('app.locales'), 'locale' => App::getLocale()]);
    }

    public function createCard()
    {
        $params = UpgradeCardCreatorProvider::processParams($_POST);
        $files = UpgradeCardCreatorProvider::processFiles($_FILES);
        $output = UpgradeCardCreatorProvider::createCard($params, $files, Session::getId());
        return response()->json($output);
    }
}

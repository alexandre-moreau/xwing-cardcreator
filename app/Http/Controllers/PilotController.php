<?php

namespace App\Http\Controllers;

use App\Providers\PilotCardCreatorProvider;
use App\Providers\ShipStatProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class PilotController extends Controller
{
    public function index()
    {
        return view('pilot.index', ['locales' => config('app.locales'), 'locale' => App::getLocale()]);
    }

    public function shipData(string $shipName)
    {
        return response()->json(ShipStatProvider::shipStats($shipName));
    }

    public function createCard()
    {
        $params = PilotCardCreatorProvider::processParams($_POST);
        $files = PilotCardCreatorProvider::processFiles($_FILES);
        $output = PilotCardCreatorProvider::createCard($params, $files, Session::getId());
        return response()->json($output);
    }
}

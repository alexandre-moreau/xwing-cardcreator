<?php

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;

Route::redirect('/', '/' . Lang::getLocale());

Route::get('/api/v1/json/upgrades', 'ApiController@upgradesJsonGet');
Route::get('/api/v1/file/upgrades', 'ApiController@upgradesFileGet');
Route::post('/api/v1/json/upgrades', 'ApiController@upgradesJsonPost');
Route::post('/api/v1/file/upgrades', 'ApiController@upgradesFilePost');
Route::get('/api/v1/json/shipdata/{ship}', 'PilotController@shipData');

Route::get('/{locale}/about', 'SiteController@about')->name('about');
Route::get('/{locale}/about/cookies', 'SiteController@aboutCookies')->name('cookies');

Route::get('/{locale}/contact', 'SiteController@contact')->name('contact');
Route::post('/contact', 'SiteController@postmessage');

Route::get('/{locale}/upgrades', 'UpgradeController@index')->name('upgrades');
Route::get('/{locale}/pilots', 'PilotController@index')->name('pilots');
Route::get('/{locale}/gallery', 'SiteController@gallery')->name('gallery');
Route::post('/upgrades', 'UpgradeController@createCard');
Route::post('/pilots', 'PilotController@createCard');
Route::post('/publish', 'SiteController@publishCard');

Route::get('/{locale}', 'SiteController@index');

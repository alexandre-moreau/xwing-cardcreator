<div class="col s12 l4">
    <div id="outputImageContainer" class="center">
        <div class="center">
            <img src="/img/card-example-upgrade-0.png" style="width: 100%; max-width: 418px;">
            <br/>
        </div>
        <div class="center">
            <div class="btn tooltipped" data-position="bottom" data-delay="35" data-tooltip="Download">
                <a download="" href="" style="color: #ffffff;"><i class="material-icons">file_download</i></a>
            </div>

            <div class="btn reset tooltipped" data-position="bottom" data-delay="35" data-tooltip="Clear">
                <i class="material-icons">clear</i>
            </div>

            <div class="btn askSubmit tooltipped" data-position="bottom" data-delay="35" data-tooltip="Publish">
                <i class="material-icons">publish</i>
            </div>
            <div id="cardLoader" class="preloader-wrapper small active" style="display: none; vertical-align: middle;">
                <div class="spinner-layer spinner-blue-only">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="cardPublishForm" class="col s8 offset-s2" style="display: none;">
            <p>Enter your name to publish your card to the gallery</p>
            <label for="user-name">User name</label>
            <input type="text" name="user-name">
            <div class="btn confirmSubmit tooltipped" data-position="bottom" data-delay="35" data-tooltip="Confirm">
                <i class="material-icons">check</i>
            </div>
        </div>
{{--        <div class="preloader-wrapper active">--}}
{{--            <div class="spinner-layer spinner-blue-only">--}}
{{--                <div class="circle-clipper left">--}}
{{--                    <div class="circle"></div>--}}
{{--                </div>--}}
{{--                <div class="gap-patch">--}}
{{--                    <div class="circle"></div>--}}
{{--                </div>--}}
{{--                <div class="circle-clipper right">--}}
{{--                    <div class="circle"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
</div>

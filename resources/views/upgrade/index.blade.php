@extends('app')

@section('title', 'upgrade index')

@section('content')
    <form id="form-main">
        @csrf
        <div class="row">
            @include('upgrade.formColumn1')
            @include('upgrade.formColumn2')
            @include('upgrade.formColumn3')
        </div>
    </form>
@endsection
@section('script')
    @include('upgrade.script')
@endsection

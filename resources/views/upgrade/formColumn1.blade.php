<div class="col s12 l4">

    <div class="row">
        <div class="col s6">
            <label for="card-name">Card name</label>
            <input type="text" name="card-name">
        </div>
        <div class="col s6">
            <label for="limited">Limited <span id="limited-visualizer" class="badge" title="Limited"></span></label>
            <input type="range" id="limited" name="limited" min="1" max="4" value="4"/>
        </div>
    </div>
    <br/>

    <label for="card-image">Card image</label>
    <div class="file-field input-field">
        <div class="btn">
            <span>Browse</span>
            <input type="file" />
        </div>

        <div class="file-path-wrapper">
            <input name="card-image" class="file-path validate" type="text" placeholder="Upload file"/>
        </div>
    </div>
    <br/>

    <label for="card-type">Card type</label>

    <div class="row" id="selectCardType">
        <div class="col s12">
            <div class="col s1" symbolText="ept"><i class="xwing-miniatures-font xwing-miniatures-font-talent" symbolText="ept"></i></div>
            <div class="col s1" symbolText="force"><i class="xwing-miniatures-font xwing-miniatures-font-forcepower" symbolText="force"></i></div>
            <div class="col s1" symbolText="astromech"><i class="xwing-miniatures-font xwing-miniatures-font-astromech" symbolText="astromech"></i></div>
            <div class="col s1" symbolText="torpedoes"><i class="xwing-miniatures-font xwing-miniatures-font-torpedo" symbolText="torpedoes"></i></div>
            <div class="col s1" symbolText="missiles"><i class="xwing-miniatures-font xwing-miniatures-font-missile" symbolText="missiles"></i></div>
            <div class="col s1" symbolText="cannon"><i class="xwing-miniatures-font xwing-miniatures-font-cannon" symbolText="cannon"></i></div>
            <div class="col s1" symbolText="turret"><i class="xwing-miniatures-font xwing-miniatures-font-turret" symbolText="turret"></i></div>
            <div class="col s1" symbolText="device"><i class="xwing-miniatures-font xwing-miniatures-font-device" symbolText="device"></i></div>
            <div class="col s1" symbolText="crew"><i class="xwing-miniatures-font xwing-miniatures-font-crew" symbolText="crew"></i></div>
            <div class="col s1" symbolText="gunner"><i class="xwing-miniatures-font xwing-miniatures-font-gunner" symbolText="gunner"></i></div>
            <div class="col s1" symbolText="system"><i class="xwing-miniatures-font xwing-miniatures-font-sensor" symbolText="system"></i></div>
            <div class="col s1" symbolText="illicit"><i class="xwing-miniatures-font xwing-miniatures-font-illicit" symbolText="illicit"></i></div></div>
        <div class="col s12">
            <div class="col s1" symbolText="title"><i class="xwing-miniatures-font xwing-miniatures-font-title" symbolText="title"></i></div>
            <div class="col s1" symbolText="modification"><i class="xwing-miniatures-font xwing-miniatures-font-modification" symbolText="modification"></i></div>
            <div class="col s1" symbolText="configuration"><i class="xwing-miniatures-font xwing-miniatures-font-config" symbolText="configuration"></i></div>
        </div>
    </div>

    <br/>

    <div>
        <label for="card-text">Card text</label>
        <textarea rows="4" cols="50" name="card-text"></textarea>
        <p>** bold text **, * italic text *</p>
        <div class="row" id="selectIcons">
            <div class="col s12">
                <div class="col s1" symbolText="cha"><i class="xwing-miniatures-font xwing-miniatures-font-charge" symbolText="cha"></i></div>
                <div class="col s1" symbolText="for"><i class="xwing-miniatures-font xwing-miniatures-font-forcecharge" symbolText="for"></i></div>
                <div class="col s1" symbolText="hit"><i class="xwing-miniatures-font xwing-miniatures-font-hit" symbolText="hit"></i></div>
                <div class="col s1" symbolText="crh"><i class="xwing-miniatures-font xwing-miniatures-font-crit" symbolText="crh"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="foc"><i class="xwing-miniatures-font xwing-miniatures-font-focus" symbolText="foc"></i></div>
                <div class="col s1" symbolText="cal"><i class="xwing-miniatures-font xwing-miniatures-font-calculate" symbolText="cal"></i></div>
                <div class="col s1" symbolText="tlk"><i class="xwing-miniatures-font xwing-miniatures-font-lock" symbolText="tlk"></i></div>
                <div class="col s1" symbolText="boo"><i class="xwing-miniatures-font xwing-miniatures-font-boost" symbolText="boo"></i></div>
                <div class="col s1" symbolText="bar"><i class="xwing-miniatures-font xwing-miniatures-font-barrelroll" symbolText="bar"></i></div>
                <div class="col s1" symbolText="eva"><i class="xwing-miniatures-font xwing-miniatures-font-evade" symbolText="eva"></i></div>
                <div class="col s1" symbolText="rei"><i class="xwing-miniatures-font xwing-miniatures-font-reinforce" symbolText="rei"></i></div>
                <div class="col s1" symbolText="rel"><i class="xwing-miniatures-font xwing-miniatures-font-reload" symbolText="rel"></i></div>
                <div class="col s1" symbolText="rot"><i class="xwing-miniatures-font xwing-miniatures-font-rotatearc" symbolText="rot"></i></div>
                <div class="col s1" symbolText="jam"><i class="xwing-miniatures-font xwing-miniatures-font-jam" symbolText="jam"></i></div>
                <div class="col s1" symbolText="clk"><i class="xwing-miniatures-font xwing-miniatures-font-cloak" symbolText="clk"></i></div>
                <div class="col s1" symbolText="coo"><i class="xwing-miniatures-font xwing-miniatures-font-coordinate" symbolText="coo"></i></div>
                <div class="col s1" symbolText="sla"><i class="xwing-miniatures-font xwing-miniatures-font-slam" symbolText="sla"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="m01"><i class="xwing-miniatures-font xwing-miniatures-font-straight" symbolText="m01"></i></div>
                <div class="col s1" symbolText="m02"><i class="xwing-miniatures-font xwing-miniatures-font-bankleft" symbolText="m02"></i></div>
                <div class="col s1" symbolText="m03"><i class="xwing-miniatures-font xwing-miniatures-font-bankright" symbolText="m03"></i></div>
                <div class="col s1" symbolText="m04"><i class="xwing-miniatures-font xwing-miniatures-font-turnleft" symbolText="m04"></i></div>
                <div class="col s1" symbolText="m05"><i class="xwing-miniatures-font xwing-miniatures-font-turnright" symbolText="m05"></i></div>
                <div class="col s1" symbolText="m06"><i class="xwing-miniatures-font xwing-miniatures-font-kturn" symbolText="m06"></i></div>
                <div class="col s1" symbolText="m07"><i class="xwing-miniatures-font xwing-miniatures-font-stop" symbolText="m07"></i></div>
                <div class="col s1" symbolText="m08"><i class="xwing-miniatures-font xwing-miniatures-font-sloopleft" symbolText="m08"></i></div>
                <div class="col s1" symbolText="m09"><i class="xwing-miniatures-font xwing-miniatures-font-sloopright" symbolText="m09"></i></div>
                <div class="col s1" symbolText="m10"><i class="xwing-miniatures-font xwing-miniatures-font-dalan-bankleft" symbolText="m10"></i></div>
                <div class="col s1" symbolText="m11"><i class="xwing-miniatures-font xwing-miniatures-font-dalan-bankright" symbolText="m11"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="m12"><i class="xwing-miniatures-font xwing-miniatures-font-trollleft" symbolText="m12"></i></div>
                <div class="col s1" symbolText="m13"><i class="xwing-miniatures-font xwing-miniatures-font-trollright" symbolText="m13"></i></div>
                <div class="col s1" symbolText="m14"><i class="xwing-miniatures-font xwing-miniatures-font-ig88d-sloopleft" symbolText="m14"></i></div>
                <div class="col s1" symbolText="m15"><i class="xwing-miniatures-font xwing-miniatures-font-ig88d-sloopright" symbolText="m15"></i></div>
                <div class="col s1" symbolText="m16"><i class="xwing-miniatures-font xwing-miniatures-font-reversestraight" symbolText="m16"></i></div>
                <div class="col s1" symbolText="m17"><i class="xwing-miniatures-font xwing-miniatures-font-reversebankleft" symbolText="m17"></i></div>
                <div class="col s1" symbolText="m18"><i class="xwing-miniatures-font xwing-miniatures-font-reversebankright" symbolText="m18"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="ept"><i class="xwing-miniatures-font xwing-miniatures-font-talent" symbolText="ept"></i></div>
                <div class="col s1" symbolText="for"><i class="xwing-miniatures-font xwing-miniatures-font-forcepower" symbolText="for"></i></div>
                <div class="col s1" symbolText="ast"><i class="xwing-miniatures-font xwing-miniatures-font-astromech" symbolText="ast"></i></div>
                <div class="col s1" symbolText="tor"><i class="xwing-miniatures-font xwing-miniatures-font-torpedo" symbolText="tor"></i></div>
                <div class="col s1" symbolText="mis"><i class="xwing-miniatures-font xwing-miniatures-font-missile" symbolText="mis"></i></div>
                <div class="col s1" symbolText="can"><i class="xwing-miniatures-font xwing-miniatures-font-cannon" symbolText="can"></i></div>
                <div class="col s1" symbolText="tur"><i class="xwing-miniatures-font xwing-miniatures-font-turret" symbolText="tur"></i></div>
                <div class="col s1" symbolText="dev"><i class="xwing-miniatures-font xwing-miniatures-font-device" symbolText="dev"></i></div>
                <div class="col s1" symbolText="cre"><i class="xwing-miniatures-font xwing-miniatures-font-crew" symbolText="cre"></i></div>
                <div class="col s1" symbolText="gun"><i class="xwing-miniatures-font xwing-miniatures-font-gunner" symbolText="gun"></i></div>
                <div class="col s1" symbolText="sen"><i class="xwing-miniatures-font xwing-miniatures-font-sensor" symbolText="sen"></i></div>
                <div class="col s1" symbolText="ill"><i class="xwing-miniatures-font xwing-miniatures-font-illicit" symbolText="ill"></i></div>
                <div class="col s1" symbolText="tit"><i class="xwing-miniatures-font xwing-miniatures-font-title" symbolText="tit"></i></div>
                <div class="col s1" symbolText="mod"><i class="xwing-miniatures-font xwing-miniatures-font-modification" symbolText="mod"></i></div>
                <div class="col s1" symbolText="con"><i class="xwing-miniatures-font xwing-miniatures-font-config" symbolText="con"></i></div>
            </div>
            <div class="col s12">
                <div class="col s1" symbolText="a01"><i class="xwing-miniatures-font xwing-miniatures-font-frontarc" symbolText="a01"></i></div>
                <div class="col s1" symbolText="a02"><i class="xwing-miniatures-font xwing-miniatures-font-reararc" symbolText="a02"></i></div>
                <div class="col s1" symbolText="a03"><i class="xwing-miniatures-font xwing-miniatures-font-fullfrontarc" symbolText="a03"></i></div>
                <div class="col s1" symbolText="a04"><i class="xwing-miniatures-font xwing-miniatures-font-fullreararc" symbolText="a04"></i></div>
                <div class="col s1" symbolText="a05"><i class="xwing-miniatures-font xwing-miniatures-font-bullseyearc" symbolText="a05"></i></div>
                <div class="col s1" symbolText="a06"><i class="xwing-miniatures-font xwing-miniatures-font-singleturretarc" symbolText="a06"></i></div>
                <div class="col s1" symbolText="a07"><i class="xwing-miniatures-font xwing-miniatures-font-doubleturretarc" symbolText="a07"></i></div>
                <div class="col s1" symbolText="a08"><i class="xwing-miniatures-font xwing-miniatures-font-leftarc" symbolText="a08"></i></div>
                <div class="col s1" symbolText="a09"><i class="xwing-miniatures-font xwing-miniatures-font-rightarc" symbolText="a09"></i></div>
                <div class="col s1" symbolText="a10"><i class="xwing-miniatures-font xwing-miniatures-font-rearbullseyearc" symbolText="a10"></i></div>
            </div>
        </div>
    </div>
</div>

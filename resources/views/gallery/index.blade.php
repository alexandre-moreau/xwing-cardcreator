@extends('app')

@section('title', 'Gallery')

@section('content')
    <div class="row">
        @foreach ($cards as $path)
            <div class="col l3 m6 ms12" style="padding-top: 10px">
                <div class="card">
                    <div class="card-image">
                        <img src="{{ $path }}" alt="{{ $path }}">
{{--                        <a class="btn-floating btn-small tooltipped halfway-fab waves-effect waves-light green"--}}
{{--                           data-position="bottom"--}}
{{--                           data-tooltip="download">--}}
{{--                            <i class="material-icons">file_download</i>--}}
{{--                        </a>--}}
                    </div>
                    <div class="card-content">
                        <p>{{ ucfirst( __('by')) }} {{ explode("_", $path)[1] != '' ? explode("_", $path)[1] : 'Anonymous' }}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

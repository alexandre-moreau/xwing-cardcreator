@extends('app')

@section('title', '503')

@section('content')
    <div class="row">
        <div class="center" style="padding-top: 8em; padding-bottom: 8em;">
            <h5>The website is being upgraded. Please come back later.</h5>
        </div>
    </div>
@endsection

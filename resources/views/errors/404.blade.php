@extends('app')

@section('title', '404')

@section('content')
    <div class="row">
        <div class="center" style="padding-top: 8em; padding-bottom: 8em;">
            <h5><i>"Impossible. Perhaps the archives are incomplete."</i></h5>
            <h5>Page not found.</h5>
        </div>
    </div>
@endsection
